*** Settings ***
Library         Selenium2Library
Library         Remote  http://ffmpeg:8270  WITH NAME  ffmpeg
Library         Remote  http://xvfb:8270  WITH NAME  xvfb
Library         Remote  http://i3wm:8270  WITH NAME  i3wm
Suite Setup     Setup
Suite Teardown  Teardown

*** Variables ***
${url}       http://www.google.com

*** Testcases ***
Chrome Browser should open up
  Open Browser   ${url}   browser=chrome
  Maximize Browser Window
  Sleep    20
  Close All Browsers

Firefox Browser should open up
  Open Browser   ${url}   browser=firefox
  Maximize Browser Window
  Sleep    20
  Close All Browsers

*** Keywords ***
Setup
  Open Display
  Start Window Manager
  Start Capture

Teardown
  Stop Capture
  Stop Window Manager
  Close Display