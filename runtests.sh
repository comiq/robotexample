#!/bin/sh
mkdir capture ||true
mkdir report ||true
chmod -R g+w *

docker-compose up -d xvfb
docker-compose up -d i3wm
docker-compose up -d ffmpeg
docker-compose up robotframework
docker-compose stop
docker-compose rm -f

